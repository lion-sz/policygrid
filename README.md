
# PolicyGrid

<!-- badges: start -->
<!-- badges: end -->

This packages contains a few functions to build the policy grid.

## Installation

You can install the released version of PolicyGrid from Gitlab.
It requires the WaybackFetcheR to be installed.

``` r
library(devtools)
install_gitlab("ClF3/WaybackFetcheR")
install_gitlab("ClF3/PolicyGrid", auth_token = "_DsmUXYVPx4aGc5dB1C_")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(PolicyGrid)
## basic example code
```

